<?php
header("Cache-control: private");
include(dirname(__FILE__).'/include/config.inc.php');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

    <head>
        <title>Casterclub Streaming Calculator Pro</title>   
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=yes">           
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Cache-control" content="no-cache">
        <meta name="robots" content="index, follow">
        <link id="siteicon" rel="icon" sizes="16x16" href="http://streamcalcpro.casterclub.com/favicon.ico">
        <link rel="shortcut icon" href="http://streamcalcpro.casterclub.com/favicon.ico" type="image/x-icon">
        <meta name="google-play-app" content="app-id=com.casterclub.streamcalcpro">
        <meta name="google-chrome-app" content="iiikcodgccjoendiekefpmhaeaidohfc">
        <meta name="google-chrome-app-name" content="streaming-calculator-pro">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="StreamCalcPro">
        <link rel="apple-touch-icon" sizes="128x128" href="http://streamcalcpro.casterclub.com/images/blue_calculator-128x128.png">
        <link rel="apple-touch-icon-precomposed" sizes="128x128" href="http://streamcalcpro.casterclub.com/images/blue_calculator-128x128.png">
        <link rel="apple-touch-startup-image" href="http://streamcalcpro.casterclub.com/images/blue_calculator-128x128.png">

        <meta name="description" content="Casterclub Streaming Calculator Pro">
        <meta name="application-name" content="StreamCalcPro">
        <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
        <link href="css/streamcalc/jquery-ui-1.10.4.custom.min.css" rel="stylesheet">
        <link href="css/jquery.smartbanner.css" type="text/css" rel="stylesheet" media="screen">
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery.smartbanner.js"></script>
        <script src="http://speedof.me/api/api.js" type="text/javascript"></script>
        <script src="js/jquery-ui-1.10.4.custom.js"></script>
            <!--external html5shiv <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/html5shiv.js"><\/script>')</script>
             -->
        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
        <![endif]-->
         
<?php
 if($cal1 == 'cal_lstnrs'){
      echo '<script>
$(document).ready(function() {
  //handle support dialog and tabs
  $( "#streamcalc_tabs" ).tabs({ active: 0, heightStyle: "content" });
});
 </script>';    
 }elseif($cal2 == 'cal_bw'){
      echo '<script>
$(document).ready(function() {
  //handle support dialog and tabs
  $( "#streamcalc_tabs" ).tabs({ active: 1, heightStyle: "content" });
});
 </script>';    
 }elseif($cal4 == 'Calculate'){
     echo '<script>
$(document).ready(function() {
  //handle support dialog and tabs
  $( "#streamcalc_tabs" ).tabs({ active: 2, heightStyle: "content" });
});
 </script>';
     
 }elseif($info == 'about'){
     echo '<script>
$(document).ready(function() {
  //handle support dialog and tabs
  $( "#streamcalc_tabs" ).tabs({ active: 3, heightStyle: "content" });
});
 </script>';
     
 }else{
           echo '<script>
$(document).ready(function() {
  //handle support dialog and tabs
  $( "#streamcalc_tabs" ).tabs({ active: 0, heightStyle: "content" });
});
 </script>'; 
     
 } 
   
    
?>
<script language="javascript">
    document.onmousedown=disableclick;
    function disableclick(event) {
        if(event.button == 2){
            return false;    
        }
    }
    
$(function() {

    //Calculate Listener form.
   $('#CALCULATE_LISTENERS').submit(function(e) {
    e.preventDefault();
    $.ajax({
       type: "POST",
       url: 'api/?datacall=CalculateListeners',
       data: $(this).serialize(),
       success: function(data)
       {
           var content = $( data );
           $( "#load_calculated_listener_result" ).empty().append( content );
       }
       
       
       
       
   });
 });

  
  
      //Calculate Bandwidth Needs  form.
   $('#CALCULATE_BWNEEDS').submit(function(e) {
    e.preventDefault();
    $.ajax({
       type: "POST",
       url: 'api/?datacall=CalculateBandwidthNeeds',
       data: $(this).serialize(),
       success: function(data)
       {
           var content = $( data );
           $( "#load_calculated_bwneeds_result" ).empty().append( content );
       }
       
       
       
       
   });
 });

    //Calculate bwusage
   $('#CALC_BWUSAGE').submit(function(e) {
    e.preventDefault();
    $.ajax({
       type: "POST",
       url: 'api/?datacall=CalculateBWUsage',
       data: $(this).serialize(),
       success: function(data)
       {
           var content = $( data );
           $( "#load_calculated_bwusage_result" ).empty().append( content );
       }
       
       
       
       
   });
 });

 $( "#button_speedtest" ).button({ icons: { primary: "ui-icon-clock" } });
 $( "#rekick1" ).button({ icons: { primary: "ui-icon-reset" } });
 $( "#rekick2" ).button({ icons: { primary: "ui-icon-reset" } });
 $( "#rekick3" ).button({ icons: { primary: "ui-icon-reset" } });
 $( "#benchmark" ).button({ icons: { primary: "ui-icon-clock" } });
 $( "#calculate" ).button({ icons: { primary: "ui-icon-calculator" } });
 $( "#calculatebw" ).button({ icons: { primary: "ui-icon-calculator" } });
 $( "#calculatebwu" ).button({ icons: { primary: "ui-icon-calculator" } });
 
 $( "#sustainTime" ).slider({
      range: "max",
      min: 1,
      max: 8,
      value: 4,
      slide: function( event, ui ) {
        $( "#amount" ).val( ui.value );
      }
    });
    $( "#amount" ).val( $( "#sustainTime" ).slider( "value" ) );

//handle speed test dialoge
       $( "#dialog_st" ).dialog({
            autoOpen: false,
            position: "left top", at: "left bottom", of: "button",    
            modal: true,
            draggable: false,
            resizable: false,
            width: 796,
            height: 410,
            buttons: [
                {
                    text: "Close Benchmark Test",
                    click: function() {
                        $( this ).dialog( "close" );
                    }
                }
            ]
        })
        
      
        // Link to open the dialog
        $( "#dialog-link_st" ).click(function( event ) {
            $( "#dialog_st" ).dialog( "open" );
            event.preventDefault();
        });
        // Hover states on the static widgets
        $( "#dialog-link_st span.ui-icon" ).hover(
            function() {
                $( this ).addClass( "ui-state-hover" );
            },
            function() {
                $( this ).removeClass( "ui-state-hover" );
            }
        );
      
});


</script>
</head>
<body id="body" oncontextmenu="return false">