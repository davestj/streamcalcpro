<!--process speed test dialogue for bandwidth benchamrking-->
<div>
        <div>
            <button class="ui-btn ui-shadow ui-corner-all ui-btn-icon-left ui-icon-clock" id="benchmark" style="width:auto" id="btnStart" type="button" onclick="btnStartClick()">Start Benchmark</button><br />
            <div id="msg" ></div>  
        </div>
        <form>
<div data-role="fieldcontain">
    <div id="prgs"></div>
    <div class="container_bar" id="container_bar"><div id="progressbar" class="progressbar"></div></div> 
<p><a href="#MeasureInfo" data-rel="popup" data-transition="pop" data-icon="helpapp" class="my-tooltip-btn ui-btn ui-alt-icon ui-nodisc-icon ui-btn-inline ui-icon-helpapp ui-btn-icon-notext" title="Learn more"></a>Measure in seconds.</p>
<div data-role="popup" id="MeasureInfo" class="ui-content" data-theme="a" style="max-width:420px;">
<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>  
  <p>You can adjust this time to best suit your needs. You may choose from 1 to 6 seconds. (Default 
value is 2 seconds). For accurate results, you can set this number to 6. 
</p>
</div>                           
<input name="sustainTime" id="sustainTime" value="2" min="1" max="6" data-theme="c" data-track-theme="c"/> 
<label for="amount"></label> 
 
</div>   

<div data-role="popup" id="popoptions" data-overlay-theme="b" data-theme="c" data-dismissible="false" style="max-width:400px; width: 300px !important;">
    <div data-role="header" class="header_bar" data-theme="b">
    <h1>Settings</h1>
    </div>
    <div role="main" class="ui-content">
     
<div data-role="fieldcontain">
<legend>Available Benchmark Options</legend>

    <fieldset data-role="controlgroup">
         
        <input type="checkbox" checked="checked" name="testServerEnabled" id="testServerEnabled" class="custom" />
        <label for="testServerEnabled">Find Server</label>

        <input type="checkbox" checked="checked" name="userInfoEnabled" id="userInfoEnabled" class="custom" />
        <label for="userInfoEnabled">Show My Info</label>
        
        <input type="checkbox" checked="checked" name="latencyTestEnabled" id="latencyTestEnabled" class="custom" />
        <label for="latencyTestEnabled">Measure Latency</label>

        <input type="checkbox" checked="checked" name="uploadTestEnabled" id="uploadTestEnabled" class="custom" />
        <label for="uploadTestEnabled">Test Upload</label>

        <input type="checkbox" checked="checked" name="progress.enabled" id="progress.enabled" class="custom" />
        <label for="progress.enabled">Show Progress</label>

        <input type="checkbox" checked="checked" name="progress.verbose" id="progress.verbose" class="custom" />
        <label for="progress.verbose">Show Detail</label>
    </fieldset>
    <a href="#" class="ui-btn ui-icon-back ui-btn-icon-left ui-corner-all ui-btn-b" data-rel="back">Ok</a>
</div>

    </div>
</div>
        </form>
    </div>
    <br />




    <script type="text/javascript">
        SomApi.account = "{$Apikey}";  //your API Key here
        SomApi.domainName = "{$Apidomain}";     //your domain or sub-domain here
        SomApi.onTestCompleted = onTestCompleted;
        SomApi.onError = onError;
        SomApi.onProgress = onProgress;

        var msgDiv = document.getElementById("msg");
        var prgsDiv = document.getElementById("prgs");
        var prgsbarDiv = document.getElementById("progressbar");
       var prgscontbarDiv = document.getElementById("container_bar"); 
function btnStartClick() {
            //set config values
            SomApi.config.sustainTime = document.getElementById("sustainTime").value;
            SomApi.config.testServerEnabled = document.getElementById("testServerEnabled").checked;
            SomApi.config.userInfoEnabled = document.getElementById("userInfoEnabled").checked;
            SomApi.config.latencyTestEnabled = document.getElementById("latencyTestEnabled").checked;
            SomApi.config.uploadTestEnabled = document.getElementById("uploadTestEnabled").checked;
            SomApi.config.progress.enabled = document.getElementById("progress.enabled").checked;
            SomApi.config.progress.verbose = document.getElementById("progress.verbose").checked;

            msgDiv.innerHTML = "";
            prgsbarDiv.innerHTML = "<div class=\"container_bar\" id=\"container_bar\">";
            prgscontbarDiv.innerHTML = "<div id=\"progressbar\" class=\"progressbar\"></div></div>";
            SomApi.startTest();
        }

function onTestCompleted(testResult) {
            prgsDiv.innerHTML = "";
            prgsbarDiv.innerHTML = "";
            prgscontbarDiv.innerHTML = "";
            
           var dlData;
           var upData;
           var latData;
           var testServerData;
           var yourIp;
           var yourHostname;
         if(    testResult.download ){
             dlData = "<label>Your Download: <b>" + testResult.download + " Mbps </b></label>"; 
         }
         if( testResult.upload ){
           upData = "<label>Your Upload:  <b>" + testResult.upload + " Mbps </b></label>";  
         }else{
           upData = "";
         }
         
         if( testResult.latency ){
             latData = "<label>Network Latency:  <b>" + testResult.latency + " ms </b></label>" +
             "<label>Jitter:  <b>" + testResult.jitter + " ms </b></label>";
         }else{
            latData = "" 
         }
         
         
         if( testResult.testServer ){
             testServerData = "<label>Benchmark Server Location:  <b>" + testResult.testServer + "</b></label>";
         }else{
             testServerData  = "";
         }
         
         if( testResult.ip_address ){
             yourIp = "<label>IP:  <b>" + testResult.ip_address + "</b></label>";
         }else{
            yourIp  = "";
         }
         
         if( testResult.hostname ){
             yourHostname = "<label>Your Hostname:  <b>" + testResult.hostname + " </b></label>";
         }else{
            yourHostname = ""; 
         }
         /*  
                "<label>Jitter:  <b>" + testResult.jitter + " ms </b></label>" +
                 "<label>Benchmark Server Location:  <b>" + testResult.testServer + "</b></label>" +
                 "<label>IP:  <b>" + testResult.ip_address + "</b></label>" +
                "<label>Your Hostname:  <b>" + testResult.hostname + " </b></label>" +
                "</div>";
          */      
           msgDiv.innerHTML =  "<h4>Results</h4>" + dlData +  "" + upData + "" 
           + latData + testServerData + yourIp + yourHostname;     
        }

function onError(error) {
            msgDiv.innerHTML = "Error " + error.code + ": " + error.message;
        }

 function onProgress(progress) { 
 var progressBarWidth = progress.percentDone*$(".container_bar").width()/ 100; 
 $(".progressbar").width(progressBarWidth).html(progress.percentDone + "% ");
 
            prgsDiv.innerHTML =
                 "<h4>" +
                "Test Type: " + progress.type + "<br/>" +
                "Pass: " + progress.pass + "<br/>" +
                "Current Speed: " + progress.currentSpeed + " Mbps <br/>" +
            "</h4><br />";
        } 

    </script>
       
 </div>
{include file="footer_nav.tpl"}