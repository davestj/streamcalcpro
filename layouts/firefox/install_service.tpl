<!DOCTYPE html>
    <head>
        <title>{$ShortWebAppName} - Firefox Service Installer</title>   
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=yes">           
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Cache-control" content="no-cache">
        <meta name="robots" content="index, follow">
        
        <link id="siteicon" rel="icon" sizes="16x16" href="http://streamcalcpro.casterclub.com/favicon.ico">
        <link rel="shortcut icon" href="http://streamcalcpro.casterclub.com/favicon.ico" type="image/x-icon">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="StreamCalcPro">
        <link rel="apple-touch-icon" sizes="128x128" href="http://streamcalcpro.casterclub.com/images/blue_calculator-128x128.png">
        <link rel="apple-touch-icon-precomposed" sizes="128x128" href="http://streamcalcpro.casterclub.com/images/blue_calculator-128x128.png">
        <link rel="apple-touch-startup-image" href="http://streamcalcpro.casterclub.com/images/blue_calculator-128x128.png">        
        <meta name="google-play-app" content="app-id=com.casterclub.streamcalcpro">
        <meta name="google-chrome-app" content="iiikcodgccjoendiekefpmhaeaidohfc">
        <meta name="google-chrome-app-name" content="streaming-calculator-pro">
        <meta name="mobile-web-app-capable" content="yes">
        
        <meta name="description" content="Casterclub Streaming Calculator Pro">
        <meta name="application-name" content="StreamCalcPro">
    

<link href="{$cssPath}/jquery.smartbanner.css" type="text/css" rel="stylesheet" media="screen">
<script src="/js/jquery-1.10.2.js"></script>
<script src="{$jsPath}/jquery.smartbanner.js"></script>
<script src="/js/jquery-ui-1.10.4.custom.js"></script>



</head>
{literal}
<script>
var loc = location.href;
var baseurl = loc.substring(0,loc.lastIndexOf('/'));

var data = {
  // currently required
  "name": "{/literal}{$ShortWebAppName}{literal}",
  "iconURL": baseurl+"/images/ff_service/icon-16x16.png",
  "icon32URL": baseurl+"/images/ff_service/icon-32x32.png",
  "icon64URL": baseurl+"/images/ff_service/icon-64x64.png",
  "sidebarURL": "http://{/literal}{$Apidomain}{literal}/?from=firefox&type=service",
  "statusURL": "http://{/literal}{$Apidomain}{literal}/?from=firefox&type=status",
  "markURL": "http://{/literal}{$Apidomain}{literal}/?from=firefox&type=addbookmark&url=%{url}",
  "markedIcon": baseurl+"/images/ff_service/remove.png",
  "unmarkedIcon": baseurl+"/images/ff_service/add.png",
  // should be available for display purposes
    "description": "CasterClub Streaming Calculator, for more information please visit our website.",
    "author": "David St John @ CasterClub, Inc",
    "homepageURL": "http://casterclub.com",

  // optional
  "version": "2.4 ff service"
}

function activate(node) {
  var event = new CustomEvent("ActivateSocialFeature");
  node.setAttribute("data-service", JSON.stringify(data));
  node.dispatchEvent(event);
}


</script>
{/literal}


<body contentid="content" >

<script type="text/javascript">
        $(function () {       
        $.smartbanner({
  title: 'Streaming Calculator Pro', // What the title of the app should be in the banner (defaults to <title>)
  author: null, // What the author of the app should be in the banner (defaults to <meta name="author"> or hostname)
  price: '$0.00', // Price of the app
  appStoreLanguage: 'us', // Language code for App Store
  inAppStore: '0.00', // Text of price for iOS
  inGooglePlay: '0.00', // Text of price for Android
  inWindowsStore: 'Free as Firefox Service.', // Text of price for Windows
  icon: null, // The URL of the icon (defaults to <meta name="apple-touch-icon">)
  iconGloss: null, // Force gloss effect for iOS even for precomposed
  button: 'Install As Firefox Service.', // Text for the install button
  scale: 'auto', // Scale based on viewport size (set to 1 to disable)
  speedIn: 300, // Show animation speed of the banner
  speedOut: 400, // Close animation speed of the banner
  daysHidden: 0, // Duration to hide the banner after being closed (0 = always show banner)
  daysReminder: 90, // Duration to hide the banner after "VIEW" is clicked *separate from when the close button is clicked* (0 = always show banner)
  force: 'android' // Choose \'ios\', \'android\' or \'windows\'. Dont do a browser check, just always show this banner
  }) 
}); 

function CheckService(){
var visible = navigator.MozSocial.isVisible;
var serviceDiv = document.getElementById("servicemsg");
    if( visible = 'true' ){
        serviceDiv.innerHTML = "Active";
        alert( 'visible' );
    }else{
        serviceDiv.innerHTML = "Activate Streaming Calculator Service for Firefox.";
        alert( 'invisible' );
    }
}
</script>
<div id="content" style="width: 380px; height: 100%; top: 0; left: 0;"/>


  <!--<button onclick="CheckService()" title="Install Streaming Calculator">Install Streaming Calculator</button>

   <div id="servicemsg"></div>
  -->
  <div id="is-installed"></div>
  </body>
</html>