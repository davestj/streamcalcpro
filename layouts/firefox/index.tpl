<!-- Start Home page -->
<div data-role="page" data-theme="a" id="Home" >
    <div data-role="header" class="header_bar" data-id="headernav" data-position="fixed" >  
        <h1>{$ShortWebAppName}</h1>
    </div>

    <div data-role="main" class="ui-content" data-theme="a">
            <ul data-role="listview"  data-inset="true" data-theme="a" >
                <li data-role="list-divider" class="header_bar">Choose Calculation</li>
                <li><a href="#calulcate_listeners" data-transition="slide"><img src="/css/images/icons/users_listeners.png" width="48" height="48" alt="Listener Caclulations" class="ui-corner-none">Listeners</a></li>
                <li><a href="#calculate_bwneeds" data-transition="slide"><img src="/css/images/icons/chart_bar.png" width="48" height="48" alt="Bandwidth Forecast" class="ui-corner-none">Bandwidth Forecast</a></li>
                <li><a href="#calculate_bwusage" data-transition="slide"><img src="/css/images/icons/database_active.png" width="48" height="48" alt="Bandwidth Usage" class="ui-corner-none">Bandwidth Usage</a></li>
                <li><a href="#benchmark" data-transition="slide"><img src="/css/images/icons/gear.png" width="48" height="48" alt="Bandwidth Benchmark" class="ui-corner-none">Benchmark</a></li>
            </ul>            
    </div>
        {include file="footer_nav.tpl"}
</div>
<!-- End Home page -->


<!-- ABOUT PAGE -->
<div data-role="page" data-theme="a" id="About">
  <div data-role="header" class="header_bar" data-theme="a" data-id="headernav" data-position="fixed">
   <a href="#Home" data-role="button" data-transition="slideup" data-inline="true" data-icon="custhome" class="icon-custhome ui-nodisc-icon" data-iconpos="notext">Home</a>
    <h1>About</h1>
    <a href="#" data-rel="back" data-transition="flip" data-icon="returnhome" class="ui-nodisc-icon ui-icon-returnhome ui-btn-right" data-iconpos="notext"></a>
  </div>

  <div data-role="main" class="ui-content" data-theme="a">
<div id="About" class="about">
                       <h2>About {$ShortWebAppName}</h2>
                       <font class="author">Author: dstjohn</font><br />
                       <font class="about">Website: <a target="_blank" href="http://casterclub.com">casterclub.com</a></font><br />
                       <font class="version">Version: {$WebAppVersion} Firefox Service</font><br />
                          <!-- SOCIAL MEDIA TAB AND APP DETAILS -->
  <div id="Social" class="social_tab_content">
    <fb:login-button show-faces="true" width="200" max-rows="1"></fb:login-button>
        <br>    <br>
        <a target="_blank" href="https://www.facebook.com/dialog/pagetab?app_id={$FBAppID}&redirect_uri=https://streamcalcpro.casterclub.com/?from=facebook&type=tabapp">
            <button id="addapp">Add App To Your Facebook Page</button>
        </a>
  </div>
</div>
  </div>

        {include file="footer_nav.tpl"}
</div>
<!-- END ABOUT PAGE -->

<!-- calulcate_listeners PAGE -->
<div data-role="page" data-theme="a" id="calulcate_listeners" >
  <div data-role="header" class="header_bar" data-theme="a" data-id="headernav" data-position="fixed">
   <a href="#Home" data-role="button" data-transition="slideup" data-inline="true" data-icon="custhome" class="icon-custhome ui-nodisc-icon" data-iconpos="notext">Home</a>
   <h1>Projected</h1>
   <a href="#" data-rel="back" data-transition="flip" data-icon="returnhome" class="ui-nodisc-icon ui-icon-returnhome ui-btn-right" data-iconpos="notext"></a>
  </div>

  <div data-role="main" class="ui-content" data-theme="a">
    {include file="calculate_listeners.tpl"}
  </div>

        {include file="footer_nav.tpl"}
</div>
<!-- END  calulcate_listeners  PAGE --> 



<!-- calculate_bwneeds PAGE -->
<div data-role="page" data-theme="a" id="calculate_bwneeds">
  <div data-role="header" class="header_bar" data-theme="a" data-id="headernav" data-position="fixed">
   <a href="#Home" data-role="button" data-transition="slideup" data-inline="true" data-icon="custhome" class="icon-custhome ui-nodisc-icon" data-iconpos="notext">Home</a>
    <h1>Forecast</h1>
    <a href="#" data-rel="back" data-transition="flip" data-icon="returnhome" class="ui-nodisc-icon ui-icon-returnhome ui-btn-right" data-iconpos="notext"></a>
  </div>

  <div data-role="main" class="ui-content" data-theme="a">

    {include file="calculate_bwneeds.tpl"}
  </div>

        {include file="footer_nav.tpl"}
</div>
<!-- END  calculate_bwneeds  PAGE --> 



<!-- calculate_bwusage PAGE -->
<div data-role="page" data-theme="a" id="calculate_bwusage">
  <div data-role="header" class="header_bar" data-theme="a" data-id="headernav" data-position="fixed">
   <a href="#Home" data-role="button" data-transition="slideup" data-inline="true" data-icon="custhome" class="icon-custhome ui-nodisc-icon" data-iconpos="notext">Home</a>
    <h1>Usage</h1>
<a href="#" data-rel="back" data-transition="flip" data-icon="returnhome" class="ui-nodisc-icon ui-icon-returnhome ui-btn-right" data-iconpos="notext"></a>
  </div>

  <div data-role="main" class="ui-content" data-theme="a">
        {include file="calculate_bwusage.tpl"}
  </div>

        {include file="footer_nav.tpl"}
</div>
<!-- END  calculate_bwusage  PAGE --> 




<!-- benchmark PAGE -->
<div data-role="page" data-theme="a" id="benchmark">
  <div data-role="header" class="header_bar" data-theme="a" data-id="headernav" data-position="fixed">
   <a href="#Home" data-role="button" data-transition="slideup" data-inline="true" data-icon="custhome" class="icon-custhome ui-nodisc-icon" data-iconpos="notext">Home</a>
    <h1>Benchmark</h1>
        <a href="#popoptions" data-rel="popup" data-position-to="window"  data-transition="pop" data-icon="custsettings" class="ui-nodisc-icon icon-custsettings ui-btn-right" data-iconpos="notext"></a>
  </div>

  <div data-role="main" class="ui-content" data-theme="a">

        {include file="benchmark.tpl"}
  </div>

</div>
<!-- END  benchmarke  PAGE --> 
 

