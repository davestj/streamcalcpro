<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <title>{$ShortWebAppName} Status</title>   
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=yes">           
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Cache-control" content="no-cache">
        <meta name="robots" content="index, follow">
        <link rel="stylesheet" href="/css/mobile_style.css" type="text/css" media="all">
        <link href="/css/streamcalc/jquery-ui-1.10.4.custom.min.css" rel="stylesheet">
        <!--<link rel="stylesheet" href="/css/jquery.mobile.icons.min.css" />
        <link rel="stylesheet" href="/css/jquery.mobile.custom.structure.css" />
        <link rel="stylesheet" href="/css/jquery.mobile.streamcalcpro.css" />-->
        <script src="/js/jquery-1.10.2.js"></script>
        <script src="/js/jquery-ui-1.10.4.custom.js"></script>
        <!--<script src="/js/jquery.mobile.custom.js"></script>-->
         <script>
function OpenFFPanel(){
    window.open("http://{$Apidomain}/?from=firefox&type=service", 200, null);
    
}
$(function() {
    
$( "input[type=submit], a, button" ).button()

$( "#status_tabs").tabs({});


});
</script>
</head>
<body id="content" oncontextmenu="return false" style="overflow:hidden; width: 700px; height: 400px; margin: 0px 0px 0px 0px;">
<div id="fb-root"></div>


<script>
  window.fbAsyncInit = function() {
  FB.init({
    appId      : '{$FBAppID}',
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
  });

  // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
  // for any authentication related change, such as login, logout or session refresh. This means that
  // whenever someone who was previously logged out tries to log in again, the correct case below 
  // will be handled. 
  FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
      // The response object is returned with a status field that lets the app know the current
      // login status of the person. In this case, we're handling the situation where they 
      // have logged in to the app.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // In this case, the person is logged into Facebook, but not into the app, so we call
      // FB.login() to prompt them to do so. 
      // In real-life usage, you wouldn't want to immediately prompt someone to login 
      // like this, for two reasons:
      // (1) JavaScript created popup windows are blocked by most browsers unless they 
      // result from direct interaction from people using the app (such as a mouse click)
      // (2) it is a bad experience to be continually prompted to login upon page load.
      FB.login();
    } else {
      // In this case, the person is not logged into Facebook, so we call the login() 
      // function to prompt them to do so. Note that at this stage there is no indication
      // of whether they are logged into the app. If they aren't then they'll see the Login
      // dialog right after they log in to Facebook. 
      // The same caveats as above apply to the FB.login() call here.
      FB.login();
    }
  });
  };

  // Load the SDK asynchronously
  (function(d){
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) { return }
   js = d.createElement('script'); js.id = id; js.async = true;
   js.src = "//connect.facebook.net/en_US/all.js";
   ref.parentNode.insertBefore(js, ref);
  }(document));

  // Here we run a very simple test of the Graph API after login is successful. 
  // This testAPI() function is only called in those cases. 
  function testAPI() {
    console.log('Welcome to Casterclub! ');
    FB.api('/me', function(response) {
      console.log('Good to see you, ' + response.name + '.');
    });
  }
</script>

<script>
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=666296970073355&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<header><caption>{$WebAppName}</caption></header>
<div id="status_tabs">
    <ul>
        <li><a href="#YourIp">Your Ip</a></li>
        <li><a href="#SocialContent">Social</a></li>
    </ul>
    
    <div id="YourIp"><details>Your Ip: {$ClacUserIP}</details><br /><br />
    <button onclick="OpenFFPanel()" title="Open Streaming Calculator">Open Streaming Calculator</button>
    </div>
     <div id="SocialContent" >
    <fb:login-button show-faces="true" width="200" max-rows="1"></fb:login-button>
        <br>    <br>
        <a target="_blank" href="https://www.facebook.com/dialog/pagetab?app_id={$FBAppID}&redirect_uri=https://streamcalcpro.casterclub.com/?from=facebook&type=tabapp">
        Add App To Your Facebook Page
        </a>
        <div class="fb-like" data-href="https://streamcalcpro.casterclub.com" data-width="24" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
        
   </div> 

</div>




 
  </body>
  
  </html>