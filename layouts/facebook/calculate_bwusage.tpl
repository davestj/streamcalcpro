<!--START BANDWIDTH USAGE -->                       
<div id="BandwidthUsage" class="bwusage_tab_content">
<form method="POST" id="CALC_BWUSAGE">
<center><div id="load_calculated_bwusage_result_link"></div></center>
<input type="hidden" name="universal" value="monthly_usage">
<table border="0"  width="100%" id="AutoNumber3" height="99" cellspacing="0" cellpadding="0">
   <tr>
     <td width="100%" height="68">
     <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
       <tr>
         <td width="75%">
         <label for="a">Listeners</label>
         <input type="text" name="a" value="" size="10" /></td>
       </tr>
       <tr>
         <td width="75%">
         <label for="b">Stream Bitrate</label>
         <input type="text" name="b" value="" size=20>
            <select name="dst">
                <option value="">Kbits - sec</option>
                <option value="1">KBytes - sec</option>
            </select>
            </td>
       </tr>
       <tr>
         <td width="75%">
         <label for="ts">Time Period</label>
            <select name="ts">
                 <option value="1 Month">1 Month (30 Days)</option>
                 <option value="2 Weeks">2 Weeks</option>
                 <option value="1 Day">1 Day</option>
                 <option value="1 Hour">1 Hour</option>
                 <option value="1 Minute">1 Minute</option>
                 <option value="1 Second">1 Second</option>
            </select>
            </td>
       </tr>
            <tr>
                <td width="75%"><br>
               
                <button data-iconpos="left" data-icon="calculateit" class="icon-calculateit ui-nodisc-icon ui-icon-left" id="calculatebwu" name="cal2" value="cal_usage" type="submit">Calculate</button>
                <button data-iconpos="left" data-icon="resetit" class="icon-resetit ui-nodisc-icon ui-icon-left reset_button" id="rekick3" name="reset" value="Reset" type="reset">Reset</button>
                </td>
              </tr>
     </table>
     </td>
   </tr>   
</form>
    </td>
   </tr>
 </table>
 <div data-role="popup" data-dismissible="false" id="BWUsageResultInfo" class="ui-content" data-theme="a" style="max-width:400px; width: 300px !important;">                         
 <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
 <div id="load_calculated_bwusage_result"></div>
<a href="#" data-theme="c" class="ui-btn ui-icon-back ui-btn-icon-left ui-corner-all" data-rel="back">Close</a>  
 </div>
 
</div>    
<!--END BANDWIDTH USAGE -->