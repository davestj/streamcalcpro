 <!--START LISTENER CALCS -->  
<div id="Listeners" class="lister_tab_content">                  
<form method="POST" id="CALCULATE_LISTENERS">
<input type="hidden" name="universal" value="avail_bwidth">
<table border="0" width="100%" id="AutoNumber3" height="71" cellspacing="0" cellpadding="0">
   <tr>
     <td width="100%" height="40" valign="top"><b>
     <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
       <tr>
         <td width="75%" valign="top">
            <label for="a">Upload Bandwidth</label>
            <input type="text" name="a" size="20" value="" />
         
            <select name="ult">
                <option>Kbits - sec</option>
                <option value="1">KBytes - sec</option>
                <option value="2">Mbit - sec</option>
                <option value="3">Gbit - sec</option>
                </select>
            </td>
       </tr>
       <tr>
         <td width="75%" valign="top">
         <label for="a">Stream Bitrate</label>
         <input type="text" name="b" size="20" />
            <select name="dst">
                <option>Kbits - sec</option>
                <option value="1">KBytes - sec</option>
                <option value="2">Mbit - sec</option>
                <option value="3">Gbit - sec</option>
            </select>
         </td>
       </tr>
              <tr>
                <td width="75%"><br>
                <center><div id="load_calculated_listener_result_link"></div></center>
                <button data-rel="popup" data-transition="pop" data-theme="a" style="font: 100.5%em; font-weight: bolder;" id="calculate" name="cal2" value="cal_lstnrs" type="submit">Calculate</button>
                <button data-theme="b" class="reset_button" style=" font-weight: bolder;" id="rekick1" name="reset" value="Reset" type="reset">Reset</button>
                </td>
              </tr>
       </table>
     </td>
   </tr>
</form>

    </td>
   </tr>
 </table>

<div data-role="popup" id="ListenerResultInfo" class="ui-content" data-theme="a" style="max-width:460px;">
 <div id="load_calculated_listener_result"></div>
 <a href="#" data-theme="c" class="ui-btn ui-icon-back ui-btn-icon-left ui-corner-all" data-rel="back">Close</a>  

 </div> 
                
 </div>
 <!--END LISTENER CALCS -->   