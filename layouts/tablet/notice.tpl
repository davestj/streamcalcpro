<script type="text/javascript">
        $(function () {       
        $.smartbanner({
  title: 'Streaming Calculator Pro', // What the title of the app should be in the banner (defaults to <title>)
  author: null, // What the author of the app should be in the banner (defaults to <meta name="author"> or hostname)
  price: '$0.00', // Price of the app
  appStoreLanguage: 'us', // Language code for App Store
  inAppStore: '0.00', // Text of price for iOS
  inGooglePlay: '0.00', // Text of price for Android
  inWindowsStore: 'Free in the chrome app store.', // Text of price for Windows
  icon: null, // The URL of the icon (defaults to <meta name="apple-touch-icon">)
  iconGloss: null, // Force gloss effect for iOS even for precomposed
  button: 'Install in chrome app store.', // Text for the install button
  scale: 'auto', // Scale based on viewport size (set to 1 to disable)
  speedIn: 300, // Show animation speed of the banner
  speedOut: 400, // Close animation speed of the banner
  daysHidden: 15, // Duration to hide the banner after being closed (0 = always show banner)
  daysReminder: 90, // Duration to hide the banner after "VIEW" is clicked *separate from when the close button is clicked* (0 = always show banner)
  force: 'windows' // Choose \'ios\', \'android\' or \'windows\'. Dont do a browser check, just always show this banner
  }) 
});
</script>