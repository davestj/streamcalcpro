<!-- Start Home page -->
<div data-role="page" data-theme="a" id="Home" >
    <div data-role="header" class="header_bar" data-position="fixed" >
    <a href="#Home" data-role="button" data-inline="true" data-icon="home" data-iconpos="notext">Home</a>
        <h1>{$ShortWebAppName}</h1>
    </div>

    <div data-role="main" class="ui-content" data-theme="a">
            <ul data-role="listview"  data-inset="true" data-theme="a" >
                <li data-role="list-divider" class="header_bar">Choose Calculation</li>
                <li><a href="#calulcate_listeners" data-transition="slidedown">Listeners</a></li>
                <li><a href="#calculate_bwneeds" data-transition="slidedown">Bandwidth Needs</a></li>
                <li><a href="#calculate_bwusage" data-transition="slidedown">Bandwidth Usage</a></li>
                <li><a href="#benchmark" data-transition="slidedown">Benchmark</a></li>
            </ul>            
    </div>
        {include file="footer_nav.tpl"}
</div>
<!-- End Home page -->


<!-- ABOUT PAGE -->
<div data-role="page" data-theme="a" id="About">
  <div data-role="header" class="header_bar" data-theme="a" >
   <a href="#Home" data-role="button" data-transition="slideup" data-inline="true" data-icon="home" data-iconpos="notext">Home</a>
    <h1>About</h1>
  </div>

  <div data-role="main" class="ui-content" data-theme="a">
<div id="About" class="about">
                       <h2>About {$ShortWebAppName}</h2>
                       <font class="author">Author: dstjohn</font><br />
                       <font class="about">Website: <a target="_blank" href="http://casterclub.com">casterclub.com</a></font><br />
                       <font class="version">Version: 2.0 Tablet</font><br />
</div>
  </div>

        {include file="footer_nav.tpl"}
</div>
<!-- END ABOUT PAGE -->

<!-- calulcate_listeners PAGE -->
<div data-role="page" data-theme="a" id="calulcate_listeners">
  <div data-role="header" class="header_bar" data-theme="a">
   <a href="#Home" data-role="button" data-transition="slideup" data-inline="true" data-icon="home" data-iconpos="notext">Home</a>
    <h1>Projected</h1>
  </div>

  <div data-role="main" class="ui-content" data-theme="a">
    {include file="calculate_listeners.tpl"}
  </div>

        {include file="footer_nav.tpl"}
</div>
<!-- END  calulcate_listeners  PAGE --> 



<!-- calculate_bwneeds PAGE -->
<div data-role="page" data-theme="a" id="calculate_bwneeds">
  <div data-role="header" class="header_bar" data-theme="a">
   <a href="#Home" data-role="button" data-transition="slideup" data-inline="true" data-icon="home" data-iconpos="notext">Home</a>
    <h1>Forecast</h1>
  </div>

  <div data-role="main" class="ui-content" data-theme="a">
    {include file="calculate_bwneeds.tpl"}
  </div>

        {include file="footer_nav.tpl"}
</div>
<!-- END  calculate_bwneeds  PAGE --> 



<!-- calculate_bwusage PAGE -->
<div data-role="page" data-theme="a" id="calculate_bwusage">
  <div data-role="header" class="header_bar" data-theme="a">
   <a href="#Home" data-role="button" data-transition="slideup" data-inline="true" data-icon="home" data-iconpos="notext">Home</a>
    <h1>Usage</h1>
  </div>

  <div data-role="main" class="ui-content" data-theme="a">
        {include file="calculate_bwusage.tpl"}
  </div>

        {include file="footer_nav.tpl"}
</div>
<!-- END  calculate_bwusage  PAGE --> 




<!-- benchmark PAGE -->
<div data-role="page" data-theme="a" id="benchmark">
  <div data-role="header" class="header_bar" data-theme="a">
   <a href="#Home" data-role="button" data-transition="slideup" data-inline="true" data-icon="home" data-iconpos="notext">Home</a>
    <h1>Benchmark</h1>
    <a href="#popoptions" data-rel="popup" data-position-to="window" data-transition="pop" 
class="ui-btn ui-corner-all ui-icon-plus ui-btn-icon-right">Options</a>
  </div>

  <div data-role="main" class="ui-content" data-theme="a">
        {include file="benchmark.tpl"}
  </div>

</div>
<!-- END  benchmarke  PAGE --> 
 

