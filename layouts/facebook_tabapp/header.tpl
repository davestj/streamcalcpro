<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

    <head>
        <title>Casterclub Streaming Calculator Pro</title>   
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=yes">           
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Cache-control" content="no-cache">
        <meta name="robots" content="index, follow">
        <link id="siteicon" rel="icon" sizes="16x16" href="http://streamcalcpro.casterclub.com/favicon.ico">
        <link rel="shortcut icon" href="http://streamcalcpro.casterclub.com/favicon.ico" type="image/x-icon">
        <meta name="google-play-app" content="app-id=com.casterclub.streamcalcpro">
        <meta name="google-chrome-app" content="iiikcodgccjoendiekefpmhaeaidohfc">
        <meta name="google-chrome-app-name" content="streaming-calculator-pro">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="StreamCalcPro">
        <link rel="apple-touch-icon" sizes="128x128" href="http://streamcalcpro.casterclub.com/images/blue_calculator-128x128.png">
        <link rel="apple-touch-icon-precomposed" sizes="128x128" href="http://streamcalcpro.casterclub.com/images/blue_calculator-128x128.png">
        <link rel="apple-touch-startup-image" href="http://streamcalcpro.casterclub.com/images/blue_calculator-128x128.png">

        <meta name="description" content="Casterclub Streaming Calculator Pro">
        <meta name="application-name" content="StreamCalcPro">
        <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
        <link href="/css/streamcalc/jquery-ui-1.10.4.custom.min.css" rel="stylesheet">
        <link href="/css/jquery.smartbanner.css" type="text/css" rel="stylesheet" media="screen">
        <script src="/js/jquery-1.10.2.js"></script>
        <script src="/js/jquery.smartbanner.js"></script>
        <!--<script src="http://speedof.me/api/api.js" type="text/javascript"></script>-->
        <script src="//speedof.me/api/api.js" type="text/javascript"></script>
        <script src="/js/jquery-ui-1.10.4.custom.js"></script>
            <!--external html5shiv <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/html5shiv.js"><\/script>')</script>
             -->
        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
        <![endif]-->
{literal}
<script language="javascript">
    document.onmousedown=disableclick;
    function disableclick(event){
  if(event.button==2)
   {
     return false;    
   }
}
</script>

<script>
$(function() {

$( "#streamcalc_tabs" ).tabs({ active: 0, heightStyle: "content" });

    //Calculate Listener form.
   $('#CALCULATE_LISTENERS').submit(function(e) {
    e.preventDefault();
    $.ajax({
       type: "POST",
       url: 'api/?datacall=CalculateListeners',
       data: $(this).serialize(),
       success: function(data)
       {
           var content = $( data );
           $( "#load_calculated_listener_result" ).empty().append( content );
       }
       
       
       
       
   });
 });

  
  
      //Calculate Bandwidth Needs  form.
   $('#CALCULATE_BWNEEDS').submit(function(e) {
    e.preventDefault();
    $.ajax({
       type: "POST",
       url: 'api/?datacall=CalculateBandwidthNeeds',
       data: $(this).serialize(),
       success: function(data)
       {
           var content = $( data );
           $( "#load_calculated_bwneeds_result" ).empty().append( content );
       }
       
       
       
       
   });
 });

    //Calculate bwusage
   $('#CALC_BWUSAGE').submit(function(e) {
    e.preventDefault();
    $.ajax({
       type: "POST",
       url: 'api/?datacall=CalculateBWUsage',
       data: $(this).serialize(),
       success: function(data)
       {
           var content = $( data );
           $( "#load_calculated_bwusage_result" ).empty().append( content );
       }
       
       
       
       
   });
 });

 $( "#button_speedtest" ).button({ icons: { primary: "ui-icon-clock" } });
  $( "#addapp" ).button({ icons: { primary: "ui-icon-add" } });
 $( "#rekick1" ).button({ icons: { primary: "ui-icon-reset" } });
 $( "#rekick2" ).button({ icons: { primary: "ui-icon-reset" } });
 $( "#rekick3" ).button({ icons: { primary: "ui-icon-reset" } });
 $( "#benchmark" ).button({ icons: { primary: "ui-icon-clock" } });
 $( "#calculate" ).button({ icons: { primary: "ui-icon-calculator" } });
 $( "#calculatebw" ).button({ icons: { primary: "ui-icon-calculator" } });
 $( "#calculatebwu" ).button({ icons: { primary: "ui-icon-calculator" } });
 
 $( "#sustainTime" ).slider({
      range: "max",
      min: 1,
      max: 8,
      value: 4,
      slide: function( event, ui ) {
        $( "#amount" ).val( ui.value );
      }
    });
    $( "#amount" ).val( $( "#sustainTime" ).slider( "value" ) );

//handle speed test dialoge
       $( "#dialog_st" ).dialog({
            autoOpen: false,
            position: "left top", at: "left bottom", of: "button",    
            modal: true,
            draggable: false,
            resizable: false,
            width: 810,
            height: 410,
            buttons: [
                {
                    text: "Close Benchmark Test",
                    click: function() {
                        $( this ).dialog( "close" );
                    }
                }
            ]
        })
        
      
        // Link to open the dialog
        $( "#dialog-link_st" ).click(function( event ) {
            $( "#dialog_st" ).dialog( "open" );
            event.preventDefault();
        });
        // Hover states on the static widgets
        $( "#dialog-link_st span.ui-icon" ).hover(
            function() {
                $( this ).addClass( "ui-state-hover" );
            },
            function() {
                $( this ).removeClass( "ui-state-hover" );
            }
        );
      
});


</script>


    </head>
    <body id="body" oncontextmenu="return false">
{/literal}

<div id="fb-root"></div>
{literal}
<script>
  window.fbAsyncInit = function() {
  FB.init({
    appId      : '{/literal}{$FBAppID}{literal}',
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
  });

  // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
  // for any authentication related change, such as login, logout or session refresh. This means that
  // whenever someone who was previously logged out tries to log in again, the correct case below 
  // will be handled. 
  FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
      // The response object is returned with a status field that lets the app know the current
      // login status of the person. In this case, we're handling the situation where they 
      // have logged in to the app.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // In this case, the person is logged into Facebook, but not into the app, so we call
      // FB.login() to prompt them to do so. 
      // In real-life usage, you wouldn't want to immediately prompt someone to login 
      // like this, for two reasons:
      // (1) JavaScript created popup windows are blocked by most browsers unless they 
      // result from direct interaction from people using the app (such as a mouse click)
      // (2) it is a bad experience to be continually prompted to login upon page load.
      FB.login();
    } else {
      // In this case, the person is not logged into Facebook, so we call the login() 
      // function to prompt them to do so. Note that at this stage there is no indication
      // of whether they are logged into the app. If they aren't then they'll see the Login
      // dialog right after they log in to Facebook. 
      // The same caveats as above apply to the FB.login() call here.
      FB.login();
    }
  });
  };

  // Load the SDK asynchronously
  (function(d){
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement('script'); js.id = id; js.async = true;
   js.src = "//connect.facebook.net/en_US/all.js";
   ref.parentNode.insertBefore(js, ref);
  }(document));

  // Here we run a very simple test of the Graph API after login is successful. 
  // This testAPI() function is only called in those cases. 
  function testAPI() {
    console.log('Welcome to Casterclub! ');
    FB.api('/me', function(response) {
      console.log('Good to see you, ' + response.name + '.');
    });
  }
</script>
{/literal}
