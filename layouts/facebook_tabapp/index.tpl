<table border="0" width="810px" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="100%">
<header><h3><center>{$WebAppName}</center></h3></header>
</td>
</tr>
<tr><td valign="top">
                  <div id="streamcalc_tabs">
                             <ul>
                                <li><a href="#Listeners"><span>Listener Calculations</span></a></li>
                                <li><a href="#BandwidthNeeds">Bandwidth Needs</a></li>
                                <li><a href="#BandwidthUsage">Bandwidth Usage</a></li>
                                <li><a href="api/?datacall=about&fbtab=true"><span>About<span></a></li>
                             </ul>
                                              
 <!--START LISTENER CALCS -->  
<div id="Listeners" class="lister_tab_content">                  
<form method="POST" id="CALCULATE_LISTENERS">
<input type="hidden" name="universal" value="avail_bwidth">
<table border="0" width="100%" id="AutoNumber3" height="71" cellspacing="0" cellpadding="0">
   <tr>
     <td width="100%" height="40" valign="top"><b>
     <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
       <tr>
         <td width="25%" align="right">
         <p>Upload Bandwidth:</p></td>
         <td width="75%">
            <input type="text" name="a" size="20">
         
            <select name="ult">
                <option>Kbits - sec</option>
                <option value="1">KBytes - sec</option>
                <option value="2">Mbit - sec</option>
                <option value="3">Gbit - sec</option>
                </select>
            </td>
       </tr>
       <tr>
         <td width="25%" align="right">
         <p>Bitrate:</p>
         </td>
         <td width="75%">

         <input type="text" name="b" size=20>
            <select name="dst">
                <option>Kbits - sec</option>
                <option value="1">KBytes - sec</option>
                <option value="2">Mbit - sec</option>
                <option value="3">Gbit - sec</option>
            </select>
         </td>
       </tr>
              <tr>
                <td width="25%" align="right"></td>
                <td width="75%"><br>
                <button style="font: 70.5%em;" id="calculate" name="cal2" value="cal_lstnrs" type="submit">Calculate</button>
                <button style="font: 70.5%em;" id="rekick1" name="reset" value="Reset" type="reset">Reset</button>
                </td>
              </tr>
       </table>
     </td>
   </tr>
</form>

    </td>
   </tr>
 </table>
 
 <div id="load_calculated_listener_result"></div>                  
 </div>
 <!--END LISTENER CALCS -->                       
                        
                        
<!--START BANDWIDTH NEEDS-->                        
 <div id="BandwidthNeeds" class="bw_needs_tab_content">
<form method="POST" id="CALCULATE_BWNEEDS">
<input type="hidden" name="universal" value="avail_bwidth">
<table border="0" width="100%" id="AutoNumber3" height="71" cellspacing="0" cellpadding="0">
   <tr>
     <td width="100%" height="40" valign="top">
     <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
       <tr>
         <td width="25%" align="right">
         <p>Listener Count:</p>
         </td>
         <td width="75%">
         <input type=text name="a" value="" size="20"></b></font></td>
       </tr>
       <tr>
         <td width="25%" align="right">
         <p>Bitrate of Stream:</p>
         </td>
         <td width="75%">
            <input type="text" name="b" value="" size="20">
            <select name="dst">
                <option>Kbits - sec</option>
                <option value="1">KBytes - sec</option>
            </select>
         </td>
       </tr>
            <tr>
                <td width="25%" align="right"></td>
                <td width="75%"><br>
                <button style="font: 70.5%em;" id="calculatebw" name="cal2" value="cal_bw" type="submit">Calculate</button>
                <button style="font: 70.5%em;" id="rekick2" name="reset" value="Reset" type="reset">Reset</button>
                </td>
              </tr>
       </table>
     </td>
   </tr>
</form>

    </td>
   </tr>
 </table>
 
 <div id="load_calculated_bwneeds_result"></div>                   
 </div>
<!--END BANDWIDTH NEEDS-->


       
<!--START BANDWIDTH USAGE -->                       
<div id="BandwidthUsage" class="bwusage_tab_content">
<form method="POST" id="CALC_BWUSAGE">
<input type="hidden" name="universal" value="monthly_usage">
<table border="0"  width="100%" id="AutoNumber3" height="99" cellspacing="0" cellpadding="0">
   <tr>
     <td width="100%" height="68">
     <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
       <tr>
         <td width="25%" align="right">
         <p>Listener Count:</p>
         </td>
         <td width="75%">
         <input type="text" name="a" value="" size=10></td>
       </tr>
       <tr>
         <td width="25%" align="right">
         <p>Stream Bitrate:</p>
         </td>
         <td width="75%">
         <input type="text" name="b" value="" size=20>
            <select name="dst">
                <option value="">Kbits - sec</option>
                <option value="1">KBytes - sec</option>
            </select>
            </td>
       </tr>
       <tr>
         <td width="25%" align="right">
            <p>Time Period:</p>
         </td>
         <td width="75%">
            <select name="ts">
                 <option value="1 Month">1 Month (30 Days)</option>
                 <option value="2 Weeks">2 Weeks</option>
                 <option value="1 Day">1 Day</option>
                 <option value="1 Hour">1 Hour</option>
                 <option value="1 Minute">1 Minute</option>
                 <option value="1 Second">1 Second</option>
            </select>
            </td>
       </tr>
            <tr>
                <td width="25%" align="right"></td>
                <td width="75%"><br>
                <button style="font: 70.5%em;" id="calculatebwu" name="cal2" value="cal_usage" type="submit">Calculate</button>
                <button style="font: 70.5%em;" id="rekick3" name="reset" value="Reset" type="reset">Reset</button>
                </td>
              </tr>
     </table>
     </td>
   </tr>   
</form>
    </td>
   </tr>
 </table>                         
<div id="load_calculated_bwusage_result"></div>
</div>    
<!--END BANDWIDTH USAGE -->


<!--process speed test dialogue for bandwidth benchamrking-->
<!--<a href="#" id="dialog-link_st"><button id="button_speedtest">Benchmark</button></a>-->


<div id="dialog_st" title="Bandwidth Benchmark Test">
<div style="float:left; margin-right:30px">
        <h3>Configuration & Settings</h3>
        <div style="clear:both;"><br />
            <button id="benchmark" style="width:auto" id="btnStart" type="button" onclick="btnStartClick()">Start Benchmark</button><br />
        </div>
        <br />
        <form>
            <!--Faster <input type="range" id="sustainTime" min="1" max="8" value="4"> Accurate<br /><br />-->
            <p>
                <label for="amount">Benchmark Duration in seconds</label>
                <input type="text" id="amount" style="border:0; font-weight:bolder !important; font: 90.5%em !important;">
            </p>
            <div id="sustainTime"></div><br />
            <input type="checkbox" id="testServerEnabled" checked="checked">Find Test Server<br />
            <input type="checkbox" id="userInfoEnabled" checked="checked">Find User Info<br />
            <input type="checkbox" id="latencyTestEnabled" checked="checked">Do Latency Test<br />
            <input type="checkbox" id="uploadTestEnabled" checked="checked">Do Upload Test<br />
            <input type="checkbox" id="progress.enabled" checked="checked">Show Progress<br />
            <input type="checkbox" id="progress.verbose" checked="checked">Verbose Progress<br /> 
        </form>
    </div><br />
    <div class="container_bar"><div class="progressbar"></div></div>
    <div id="prgs" style="float:left; margin-right:30px"></div>
    <div id="msg" style="float:left;"></div>

    <script type="text/javascript">
        SomApi.account = "{$Apikey}";  //your API Key here
        SomApi.domainName = "{$Apidomain}";     //your domain or sub-domain here
        SomApi.onTestCompleted = onTestCompleted;
        SomApi.onError = onError;
        SomApi.onProgress = onProgress;

        var msgDiv = document.getElementById("msg");
        var prgsDiv = document.getElementById("prgs");

function btnStartClick() {
            //set config values
            SomApi.config.sustainTime = document.getElementById("amount").value;
            SomApi.config.testServerEnabled = document.getElementById("testServerEnabled").checked;
            SomApi.config.userInfoEnabled = document.getElementById("userInfoEnabled").checked;
            SomApi.config.latencyTestEnabled = document.getElementById("latencyTestEnabled").checked;
            SomApi.config.uploadTestEnabled = document.getElementById("uploadTestEnabled").checked;
            SomApi.config.progress.enabled = document.getElementById("progress.enabled").checked;
            SomApi.config.progress.verbose = document.getElementById("progress.verbose").checked;

            msgDiv.innerHTML = "<h3>--------------- Benchmark Results ---------------</h3><h4>" +
                "Benchmark test in progress. Please wait...</h4>";
            SomApi.startTest();
        }

function onTestCompleted(testResult) {
            msgDiv.innerHTML = "<h3>----------Benchmark Results ---------------</h3><h4><textarea name=\"textarea\" cols=\"48\" rows=\"8\">" +
                "Your Download: " + testResult.download + " Mbps\n" +
                "Your Upload: " + testResult.upload + " Mbps \n" +
                "Network Latency: " + testResult.latency + " ms \n" +
                "Jitter: " + testResult.jitter + " ms \n" +
                "Benchmark Server Location: " + testResult.testServer + "\n" +
                "IP: " + testResult.ip_address + "\n" +
                "Your Hostname: " + testResult.hostname + "\n" +
            "</textarea></h4>";
        }

function onError(error) {
            msgDiv.innerHTML = "Error " + error.code + ": " + error.message;
        }

 function onProgress(progress) { 
 var progressBarWidth = progress.percentDone*$(".container_bar").width()/ 100; 
 $(".progressbar").width(progressBarWidth).html(progress.percentDone + "% ");
 
            prgsDiv.innerHTML =
                 "<h4>" +
                "Test Type: " + progress.type + "<br/>" +
                "Pass: " + progress.pass + "<br/>" +
                "Current Speed: " + progress.currentSpeed + " Mbps <br/>" +
            "</h4><br />";
        } 

    </script>
 </div>
 </div>
 