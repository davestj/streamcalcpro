<?php
header("Cache-control: private");
include(dirname(__FILE__).'/include/config.inc.php');

$AppFrom    = (string) $_REQUEST["from"];
$AppType    = (string) $_REQUEST["type"];


switch($AppFrom){
   //Firefox app framework
    case firefox:
        if($AppType == 'service'){
            $layout = new AppLayout('layouts/firefox',false,false);
            //make global smarty assignments
                 $layout->assign("TheUserAgent", $TheUagent);
                 $layout->assign("WebAppName", $AppName);
                 $layout->assign("ShortWebAppName", $ShortAppName);
                 $layout->assign("ClacHostName", $CalcHost);
                 $layout->assign("ClacUserIP", $UserIP);
                 $layout->assign("CurrentDateTime", $alld);
                 $layout->assign("FBAppID", $FBApiAppID);
                 $layout->assign("FBAppSecretKey", $FBApiKey);
                 $layout->assign("GoogleAnylitics", $google_analytics_html);
                 $layout->assign("Apidomain", $ApiDomain);
                 $layout->assign("Apikey", $ApiKey);
                 
                 $layout->display("header.tpl");
                 $layout->display("index.tpl");
                 $layout->display("footer.tpl");
        }elseif($AppType == 'status'){
            $layout = new AppLayout('layouts/firefox',false,false);
            $layout->display("service_status.tpl");
        }
    break;

    //Chrome app framework
    case chrome:
        if($AppType == 'app'){
            $layout = new AppLayout('layouts/chrome',false,false);
            //make global smarty assignments
                 $layout->assign("TheUserAgent", $TheUagent);
                 $layout->assign("WebAppName", $AppName);
                 $layout->assign("ShortWebAppName", $ShortAppName);
                 $layout->assign("ClacHostName", $CalcHost);
                 $layout->assign("ClacUserIP", $UserIP);
                 $layout->assign("CurrentDateTime", $alld);
                 $layout->assign("FBAppID", $FBApiAppID);
                 $layout->assign("FBAppSecretKey", $FBApiKey);
                 $layout->assign("GoogleAnylitics", $google_analytics_html);
                 $layout->assign("Apidomain", $ApiDomain);
                 $layout->assign("Apikey", $ApiKey);
                 
                 $layout->display("header.tpl");
                 $layout->display("index.tpl");
                 $layout->display("footer.tpl");
        }
    break;

    //Android hybrid native app framework
    case android:
        if($AppType == 'app'){
            $layout = new AppLayout('layouts/android',false,false);
            //make global smarty assignments
            $layout->assign("TheUserAgent", $TheUagent);
            $layout->assign("WebAppName", $AppName);
            $layout->assign("ShortWebAppName", $ShortAppName);
            $layout->assign("ClacHostName", $CalcHost);
            $layout->assign("ClacUserIP", $UserIP);
            $layout->assign("CurrentDateTime", $alld);
            $layout->assign("FBAppID", $FBApiAppID);
            $layout->assign("FBAppSecretKey", $FBApiKey);
            $layout->assign("GoogleAnylitics", $google_analytics_html);
            $layout->assign("Apidomain", $ApiDomain);
            $layout->assign("Apikey", $ApiKey);
            
            $layout->display("header.tpl");
            $layout->display("index.tpl");
            $layout->display("footer.tpl");
        }
        break;

    //Facebook app
    case facebook:
        if($AppType == 'app'){
            $layout = new AppLayout('layouts/facebook',false,false);
            //make global smarty assignments
            $layout->assign("TheUserAgent", $TheUagent);
            $layout->assign("WebAppName", $AppName);
            $layout->assign("ShortWebAppName", $ShortAppName);
            $layout->assign("ClacHostName", $CalcHost);
            $layout->assign("ClacUserIP", $UserIP);
            $layout->assign("CurrentDateTime", $alld);
            $layout->assign("FBAppID", $FBApiAppID);
            $layout->assign("FBAppSecretKey", $FBApiKey);
            $layout->assign("GoogleAnylitics", $google_analytics_html);
            $layout->assign("Apidomain", $ApiDomain);
            $layout->assign("Apikey", $ApiKey);
            
            $layout->display("fb_header.tpl");
            $layout->display("fb_index.tpl");
            $layout->display("fb_footer.tpl");
        }//Facebook tab app
        elseif($AppType == 'tabapp'){
            $layout = new AppLayout('layouts/facebook_tabapp',false,false);
            //make global smarty assignments
            $layout->assign("TheUserAgent", $TheUagent);
            $layout->assign("WebAppName", $AppName);
            $layout->assign("ShortWebAppName", $ShortAppName);
            $layout->assign("ClacHostName", $CalcHost);
            $layout->assign("ClacUserIP", $UserIP);
            $layout->assign("CurrentDateTime", $alld);
            $layout->assign("FBAppID", $FBApiAppID);
            $layout->assign("FBAppSecretKey", $FBApiKey);
            $layout->assign("GoogleAnylitics", $google_analytics_html);
            $layout->assign("Apidomain", $ApiDomain);
            $layout->assign("Apikey", $ApiKey);
            
            $layout->display("header.tpl");
            $layout->display("index.tpl");
            $layout->display("footer.tpl");
        }
        break;


   //Mobile app framework
    case mobile:
        if($AppType == 'app'){
            $layout = new AppLayout('layouts/mobile',false,false);
            //make global smarty assignments
            $layout->assign("TheUserAgent", $TheUagent);
            $layout->assign("WebAppName", $AppName);
            $layout->assign("ShortWebAppName", $ShortAppName);
            $layout->assign("ClacHostName", $CalcHost);
            $layout->assign("ClacUserIP", $UserIP);
            $layout->assign("CurrentDateTime", $alld);
            $layout->assign("FBAppID", $FBApiAppID);
            $layout->assign("FBAppSecretKey", $FBApiKey);
            $layout->assign("GoogleAnylitics", $google_analytics_html);
            $layout->assign("Apidomain", $ApiDomain);
            $layout->assign("Apikey", $ApiKey);
            
            $layout->display("header.tpl");
            $layout->display("index.tpl");
            $layout->display("footer.tpl");
        }
        break; 
    
    //Tablet app framework
    case tablet:
        if($AppType == 'app'){
            $layout = new AppLayout('layouts/tablet',false,false);
            //make global smarty assignments
            $layout->assign("TheUserAgent", $TheUagent);
            $layout->assign("WebAppName", $AppName);
            $layout->assign("ShortWebAppName", $ShortAppName);
            $layout->assign("ClacHostName", $CalcHost);
            $layout->assign("ClacUserIP", $UserIP);
            $layout->assign("CurrentDateTime", $alld);
            $layout->assign("FBAppID", $FBApiAppID);
            $layout->assign("FBAppSecretKey", $FBApiKey);
            $layout->assign("GoogleAnylitics", $google_analytics_html);
            $layout->assign("Apidomain", $ApiDomain);
            $layout->assign("Apikey", $ApiKey);
            $layout->display("header.tpl");
            $layout->display("index.tpl");
            $layout->display("footer.tpl");
        }
        break;
        
    //Nothind defined, leave it up for browser detectiion for smartbanner installer
    default:
    $layout = new AppLayout('layouts/desktop',false,false);
                //make global smarty assignments
                 $layout->assign("TheUserAgent", $TheUagent);
                 $layout->assign("WebAppName", $AppName);
                 $layout->assign("ShortWebAppName", $ShortAppName);
                 $layout->assign("ClacHostName", $CalcHost);
                 $layout->assign("ClacUserIP", $UserIP);
                 $layout->assign("CurrentDateTime", $alld);
                 $layout->assign("FBAppID", $FBApiAppID);
                 $layout->assign("FBAppSecretKey", $FBApiKey);
                 $layout->assign("GoogleAnylitics", $google_analytics_html);
                 $layout->assign("Apidomain", $ApiDomain);
                 $layout->assign("Apikey", $ApiKey);
                     
                 $layout->display("header.tpl");
                 $layout->display("index.tpl");
                 $layout->display("footer.tpl");
    
    
}


?>

