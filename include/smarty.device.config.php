<?php
//get device
$device_detected = new Mobile_Detect;

   // detect desktop-only OS: Windows or Mac
   $userAgent = $device_detected->getUserAgent();

///////////////////////////////////////////////////////////////////////
//CONFIGURE DESKTOPS 
if(strpos($userAgent,"Windows NT") !== FALSE) {
$layoutname = 'desktop';
$layoutpath = ''.$DocuRoot.'/layouts/'.$layoutname.'';
$layouturl = '/layouts/'.$layoutname.'';

$smarty->template_dir  = ''.$layoutpath.'';
$smarty->compile_dir   = ''.$layoutpath.'/templates_c';
$smarty->cache_dir     = ''.$layoutpath.'/cache';
$smarty->config_dir    = ''.$layoutpath.'/configs';
$smarty->plugins_dir   = ''.$DocuRoot.'/libs/Smarty/plugins';
$smarty->caching       = false;
$smarty->force_compile = true;
$smarty->debugging     = false; 

//global standard smarty assignments
$smarty->assign("imagePath", "$layouturl/images");
$smarty->assign("cssPath", "$layouturl/css");
$smarty->assign("jsPath", "$layouturl/js");
$smarty->assign("isWindowsDesktop", 'true');
      
   }
   else {
      $smarty->assign("isWindowsDesktop", 'false');
       
   }
   
   
   if(strpos($userAgent,"Windows NT 6") !== FALSE) {
$layoutname = 'desktop';
$layoutpath = ''.$DocuRoot.'/layouts/'.$layoutname.'';
$layouturl = '/layouts/'.$layoutname.'';

$smarty->template_dir  = ''.$layoutpath.'';
$smarty->compile_dir   = ''.$layoutpath.'/templates_c';
$smarty->cache_dir     = ''.$layoutpath.'/cache';
$smarty->config_dir    = ''.$layoutpath.'/configs';
$smarty->plugins_dir   = ''.$DocuRoot.'/libs/Smarty/plugins';
$smarty->caching       = false;
$smarty->force_compile = true;
$smarty->debugging     = false; 

//global standard smarty assignments
$smarty->assign("imagePath", "$layouturl/images");
$smarty->assign("cssPath", "$layouturl/css");
$smarty->assign("jsPath", "$layouturl/js");
$smarty->assign("isWindowsDesktop", 'true');
      $smarty->assign("isIE", 'true');
      
   }else {
      $smarty->assign("isIE", 'false');
      
   }
   
   if(strpos($userAgent,"MSIE 8") !== FALSE){
      $smarty->assign("isIE8", 'true');
      
   }else {
      $smarty->assign("isIE8", 'false');
      
   }
   
   
   if(preg_match('/\bFirefox\b/',$userAgent)){
$layoutname = 'firefox';
$layoutpath = ''.$DocuRoot.'/layouts/'.$layoutname.'';
$layouturl = '/layouts/'.$layoutname.'';

$smarty->template_dir  = ''.$layoutpath.'';
$smarty->compile_dir   = ''.$layoutpath.'/templates_c';
$smarty->cache_dir     = ''.$layoutpath.'/cache';
$smarty->config_dir    = ''.$layoutpath.'/configs';
$smarty->plugins_dir   = ''.$DocuRoot.'/libs/Smarty/plugins';
$smarty->caching       = false;
$smarty->force_compile = true;
$smarty->debugging     = false; 

//global standard smarty assignments
$smarty->assign("imagePath", "$layouturl/images");
$smarty->assign("cssPath", "$layouturl/css");
$smarty->assign("jsPath", "$layouturl/js");
      $smarty->assign("isFF", 'true');
        
   }else{
      $smarty->assign("isFF", 'false');
       
   }
   

   if(preg_match('/\bChrome\b/',$userAgent)){
$layoutname = 'desktop';
$layoutpath = ''.$DocuRoot.'/layouts/'.$layoutname.'';
$layouturl = '/layouts/'.$layoutname.'';

$smarty->template_dir  = ''.$layoutpath.'';
$smarty->compile_dir   = ''.$layoutpath.'/templates_c';
$smarty->cache_dir     = ''.$layoutpath.'/cache';
$smarty->config_dir    = ''.$layoutpath.'/configs';
$smarty->plugins_dir   = ''.$DocuRoot.'/libs/Smarty/plugins';
$smarty->caching       = false;
$smarty->force_compile = true;
$smarty->debugging     = false; 

//global standard smarty assignments
$smarty->assign("imagePath", "$layouturl/images");
$smarty->assign("cssPath", "$layouturl/css");
$smarty->assign("jsPath", "$layouturl/js");
      $smarty->assign("isCHROME", 'true');
    
   }else{
      $smarty->assign("isCHROME", 'false');
        
   }
   
   
/* MACOS FIREFOX  */    
if(strpos($userAgent,"Mac OS X") !== FALSE && preg_match('/\bFirefox\b/',$userAgent)) {
$layoutname = 'firefox';
$layoutpath = ''.$DocuRoot.'/layouts/'.$layoutname.'';
$layouturl = '/layouts/'.$layoutname.'';

$smarty->template_dir  = ''.$layoutpath.'';
$smarty->compile_dir   = ''.$layoutpath.'/templates_c';
$smarty->cache_dir     = ''.$layoutpath.'/cache';
$smarty->config_dir    = ''.$layoutpath.'/configs';
$smarty->plugins_dir   = ''.$DocuRoot.'/libs/Smarty/plugins';
$smarty->caching       = false;
$smarty->force_compile = true;
$smarty->debugging     = false; 

//global standard smarty assignments
$smarty->assign("imagePath", "$layouturl/images");
$smarty->assign("cssPath", "$layouturl/css");
$smarty->assign("jsPath", "$layouturl/js");
$smarty->assign("isMacDesktop", 'true');
      
   }
   else {
      $smarty->assign("isMacDesktop", 'false');
       
   }  
/* END MACOS FIREFOX  */     
   
   
/* LINUX FIREFOX  */  
if(strpos($userAgent,"Linux") !== FALSE && preg_match('/\bFirefox\b/',$userAgent)) {
$layoutname = 'firefox';
$layoutpath = ''.$DocuRoot.'/layouts/'.$layoutname.'';
$layouturl = '/layouts/'.$layoutname.'';

$smarty->template_dir  = ''.$layoutpath.'';
$smarty->compile_dir   = ''.$layoutpath.'/templates_c';
$smarty->cache_dir     = ''.$layoutpath.'/cache';
$smarty->config_dir    = ''.$layoutpath.'/configs';
$smarty->plugins_dir   = ''.$DocuRoot.'/libs/Smarty/plugins';
$smarty->caching       = false;
$smarty->force_compile = true;
$smarty->debugging     = false; 

//global standard smarty assignments
$smarty->assign("imagePath", "$layouturl/images");
$smarty->assign("cssPath", "$layouturl/css");
$smarty->assign("jsPath", "$layouturl/js");
$smarty->assign("isLinuxDesktop", 'true');
      
   }
   else {
      $smarty->assign("isLinuxDesktop", 'false');
         
   }
/* end LINUX FIREFOX  */  
   
   
   
   
   
   
   
   // detect aspects of mobile devices
    if( $device_detected->isiOS() ){
       $smarty->assign("isiOs", 'true');
       $ver = "iOs version " . $device_detected->version('iPhone');
       $smarty->assign("iOsVersion", $ver);
    }else{
        $smarty->assign("isiOs", 'false');
  
    }
  
    if( $device_detected->isAndroidOS() ){
        $smarty->assign("isAndroid", 'true');
        
        $ver = "Android version " . $device_detected->version('Android');
    }else{
        $smarty->assign("isAndroid", 'false');
        
    }

    
    
    
    
    if ( $device_detected->isMobile() ) {
$layoutname = 'mobile';
$layoutpath = ''.$DocuRoot.'/layouts/'.$layoutname.'';
$layouturl = '/layouts/'.$layoutname.'';

$smarty->template_dir  = ''.$layoutpath.'';
$smarty->compile_dir   = ''.$layoutpath.'/templates_c';
$smarty->cache_dir     = ''.$layoutpath.'/cache';
$smarty->config_dir    = ''.$layoutpath.'/configs';
$smarty->plugins_dir   = ''.$DocuRoot.'/libs/Smarty/plugins';
$smarty->caching       = false;
$smarty->force_compile = true;
$smarty->debugging     = false; 

//global standard smarty assignments
$smarty->assign("imagePath", "$layouturl/images");
$smarty->assign("cssPath", "$layouturl/css");
$smarty->assign("jsPath", "$layouturl/js");
$smarty->assign("isMobile", 'true');
       
    }else{
       $smarty->assign("isMobile", 'false');
       
    }
     
    // Any tablet device.
    if( $device_detected->isTablet() ){
      $smarty->assign("isTablet", 'true');
      
    }else{
      $smarty->assign("isTablet", 'false');
     
    }
    
    
    
    
    
    
    
    
    
    
   // Assign width for mobile devices (body width defined in pixels works smoothly)
   $dev_width = 900;
   $smarty->assign("DeviceWidth", $dev_width);
   
    
    
?>