<?php
include(dirname(__FILE__).'/common.functions.php');
include(dirname(__FILE__).'/calc.functions.php');
include(dirname(__FILE__).'/mobile.detect.class.php');
include(dirname(__FILE__).'/device.config.php');
require_once(dirname(__FILE__).'/facebook.php');
$cfgdb      = dirname(__FILE__).'/../js/config.json';
$cfgdata    = ReadConfig($cfgdb);
$jdb        = dirname(__FILE__).'/../js/data.json';
$fp         = fopen($jdb,r);
$jsrc       = fread($fp,filesize($jdb));
$result     = json_decode($jsrc,true);
$changelog  = $result['ChangeLog'];

$timer_start = str_replace(" ","",microtime());
$currentday = date ( "l" );
$currentdate = date ( "j" );
$currentmonth = date ( "F" );
$currentyear = date ( "Y" );
$currenttime = date ( "h:i:s A" );

$alld = date ( "l - F - j - Y - h:i:s A" );
$zero_month_format = date ( "m/d/Y" );



//system and app settings
$UserIP          = $_SERVER['REMOTE_ADDR'];
$UserHost        = gethostbyaddr($_SERVER['REMOTE_ADDR']);
$DocuRoot        = $_SERVER["DOCUMENT_ROOT"];
$TheUagent       = $_SERVER["HTTP_USER_AGENT"];
$CalcHost        = $_SERVER["SERVER_NAME"];
$AppName         = $cfgdata['AppName'];
$ShortAppName    = $cfgdata['AppShortName'];
$AppVersion      = $cfgdata['Version'];

//initialize custom library paths
ini_set('include_path',''.$DocuRoot.'/libs/Smarty');
include('Smarty.class.php');
require_once(dirname(__FILE__).'/layout.class.php');
//$smarty = new Smarty(); 
//include(dirname(__FILE__).'/smarty.device.config.php');

 //streamcalcpro.casterclub.com
 if($CalcHost == "streamcalcpro.casterclub.com"){
     $google_analytics_html = "<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50705205-1', 'casterclub.com');
  ga('send', 'pageview');

</script>";

     $ApiDomain    = 'streamcalcpro.casterclub.com';
     $ApiKey       = 'SOM534aa20dd7a59';


 }elseif($CalcHost == "streamcalcdev.casterclub.com"){
     
     $google_analytics_html = "<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50704106-1', 'casterclub.com');
  ga('send', 'pageview');

</script>";

     $ApiDomain    = 'streamcalcdev.casterclub.com';
     $ApiKey       = 'SOM5357f8e970ab3';

 }
 
$FBApiAppID   = $cfgdata['FaceBookAppId'];
$FBApiKey     = $cfgdata['FaceBookApiKey'];

 //setup paths for templates, css, images, mobile etc.....
?>