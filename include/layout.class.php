<?php


class AppLayout extends Smarty{
     
     public $docroot;
     public $layoutname = "layouts/desktop/";
     public $plugindir  = "/var/www/lib/Smarty/plugins";
     public $value      = "";
     public $cache      = "";
     public $debuggin   = "";
     public $fullpath   = "";
     
     
    public function __construct($layout = 'layouts/desktop',$cache = false,$debug = false) {      
        parent::__construct();
        if(!isset($layout)){
            $this->layoutname = (string) "layouts/desktop";
        }else{
            $this->layoutname   = (string) "$layout";
        }
       
       $this->docroot      = (string) $_SERVER['DOCUMENT_ROOT'];
       
       $this->fullpath     = (string) ''.$this->docroot.'/'.$this->layoutname.'/';
       $this->cache        = (bool)   $cache;
       $this->debuggin     = (bool)   $debug;
       
        //make config for smarty
        $this->template_dir  = $this->fullpath."/";
        $this->compile_dir   = $this->fullpath."/templates_c/";
        $this->config_dir    = $this->fullpath."/configs/";
        $this->cache_dir     = $this->fullpath."/cache/";
        $this->plugins_dir   = $this->plugindir;
        $this->force_compile = true;
        
        if($this->cache){
            self::enable_cache();
            parent::setCaching     (self::enable_cache()); 
        }else{
            self::disable_cache();
            parent::setCaching     (self::disable_cache()); 
        }
        
        if($this->debuggin){
            self::enable_debug();
            parent::setDebugging   (self::enable_debug());
        }else{
            self::disable_debug();
            parent::setDebugging   (self::disable_debug());
        }
   
        //v3 smarty needs setters
        parent::setTemplateDir ($this->fullpath."/");
        parent::setCompileDir  ($this->fullpath."templates_c/");
        parent::setConfigDir   ($this->fullpath."configs/");
        parent::setCacheDir    ($this->fullpath."cache/");
        
       
       //define default image, css, jspath
       $this->assign("imagePath", $this->layoutname."/images");
       $this->assign("cssPath", $this->layoutname."/css");
       $this->assign("jsPath", $this->layoutname."/js");
        
        
    }
   
    private function enable_cache(){

        return $this->caching = true;    
    
    }
    
    private function disable_cache(){

        return $this->caching = false;    
        
    }
    
    protected function enable_debug(){
 
        return  $this->debugging     = true;
    }
    
    protected function disable_debug(){
 
        return   $this->debugging     = false;
    }
    
} 
                 


?>
