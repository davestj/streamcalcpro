<?php
/*
*@Author: davestj@gmail.com
*@Script: api handler for ajax calls.
*/


function __CalculateListeners(){
    $UploadBW           = $_REQUEST["a"];
    $StreamBR           = $_REQUEST["b"];
    $Caclulation        = $_REQUEST["universal"];
    $UploadType         = $_REQUEST["ult"];
    $deststrype         = $_REQUEST["dst"];
    
    echo '<div id="content"><h3>Projected Results</h3>';

if($UploadBW && $StreamBR){
            $UploadBW = preg_replace("/,/", "", $UploadBW);
            $StreamBR = preg_replace("/,/", "", $StreamBR);
            if($UploadType == 1){
                $UploadBW = $UploadBW*8;
            }elseif($UploadType == 2){
                $UploadBW = $UploadBW*1024;
            }elseif($UploadType == 3){
                $UploadBW = $UploadBW*1024*1024;
            }
            if($deststrype == 1){
                $StreamBR = $StreamBR*8;
            }elseif($deststrype == 2){
                $StreamBR = $StreamBR*1024;
            }elseif($deststrype == 3){
                $StreamBR = $StreamBR*1024*1024;
            }
            
            
          
            
            $l = ($UploadBW*0.9)/$StreamBR;
            $l = floor($l);
            $aa = $UploadBW/1048.576;
            $Gb = $aa/1073.741824;
            $l = number_format($l, "0");
            $k = number_format($UploadBW, "0");
            $m = number_format($aa, "0");
            $g = number_format($Gb, "0");
// spit out the results
echo'  
<div class="ui-widget"> 
    <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0.6em;">
        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .2em;"></span>
        <strong>Stream Bitrate</strong> '.$StreamBR.'Kbps<br><br>';
        if($m > 0){
        $meg = ''.$m.' Mbps';
        }
        if($g > 0){
        $gig = ''.$g.' Gbps';
        }
        echo'
        <strong>Available Bandwidth</strong><br>'.$k.' Kbps<br> '.$meg.'<br>'.$gig.'<br>
        <strong>Calculated Results</strong><br>You can support '.$l.' listeners<br>

        </p>
        <form method="POST" id="CLEAR_LISTENERS">
          <button data-iconpos="left" data-icon="calculateit" class="icon-calculateit ui-nodisc-icon ui-icon-left" id="clear" name="clear_cal2" value="clear_lstnrs" type="submit">Clear</button>
        </form>
    </div>

</div>
';
}else{
echo '
            <div class="ui-widget">
                    <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                        <strong>Alert:</strong> Calculations left out, please enter your calculations in..</p>
                    </div>
            </div>
';
}         
    
    echo '</div>';
}


function __CalculateBandwidthNeeds(){
    $ListenerCount      = $_REQUEST["a"];
    $StreamBR           = $_REQUEST["b"];
    $Caclulation        = $_REQUEST["universal"];
    $deststrype         = $_REQUEST["dst"];   
  echo '<div id="content"><h3>Calculated Results</h3>';
                       if($ListenerCount  && $StreamBR){
                        $ListenerCount  = preg_replace("/,/", "", $ListenerCount );
                        $StreamBR = preg_replace("/,/", "", $StreamBR);
                        
                    if ($deststrype == 1){
                        $StreamBR = $StreamBR*8;
                    }
                    
                    $l = ($ListenerCount *$StreamBR)*1.1;
                    $l = ceil($l);
                    $KB = $l/8;
                    $MB = $l/1024;
                    $GB = $MB/1024;
                    $l = number_format($l, "0");
                    $m = number_format($MB, "0");
                    $g = number_format($GB, "0");
                    if($m > 0){
                        $meg = 'or '.$m.' Mbps of bandwidth <br>';
                    }
                    if($g > 0){
                        $gig = 'or '.$g.' Gbps of bandwidth <br>';
                    }
//spit out the results
echo'
<div class="ui-widget">
    <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
        Bandwidth Forecast</strong><br> <br>
        Broadcasting @ '.$StreamBR.' Kbps <br>
        You need '.$l.' Kbps <br>
        '.$meg.' '.$gig.'
         to support '.$ListenerCount.' simultaneous listeners/viewers.
        </p>
        <form method="POST" id="CLEAR_BWNEEDS">
          <button data-iconpos="left" data-icon="calculateit" class="icon-calculateit ui-nodisc-icon ui-icon-left" id="clear" name="clear_bwneeds" value="clear_bw" type="submit">Clear</button>
        </form>
    </div>
</div>
';

}else{
// tell them if they left something out
            echo '
            <div class="ui-widget">
                    <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                        <strong>Alert:</strong> Calculations left out, please enter your calculations in..</p>
                    </div>
            </div>
            ';
}
   
      
  echo '</div>';   
}




function __CalculateBWUsage(){
    $Listeners          = $_REQUEST["a"];
    $StreamBR           = $_REQUEST["b"];
    $Caclulation        = $_REQUEST["universal"];
    $Timeslot           = $_REQUEST["ts"];
    $deststrype         = $_REQUEST["dst"];
  echo '<div id="content"><h3>Calculated Usage</h3>';
          
          
if ($Listeners && $StreamBR){
$Listeners = preg_replace("/,/", "", $Listeners);
$StreamBR = preg_replace("/,/", "", $StreamBR);
if($deststrype == 1){
$StreamBR = $StreamBR*8;
}
$l = ($Listeners*$StreamBR)*1.1;
switch($Timeslot){
    case "1 Month":
                    $l = ((($l*60)*60)*24)*30;
        break;
    case "2 Weeks":
                    $l = ((($l*60)*60)*24)*14;
        break;
    case "1 Day":
                    $l = (($l*60)*60)*24;
        break;
    case "1 Hour":
                    $l = ($l*60)*60;
        break;
    case "1 Minute":
                    $l = $l*60;
        break;
    case "1 Second":
        break;
}
$Kb = ceil($l);
$KB = $Kb/8;
$Mb = $Kb/1048.576;
$MB = $Mb/8;
$Gb = $Mb/1073.741824;
$GB = $Gb/8;
$Kb = number_format($Kb, "0");
$KB = number_format($KB, "0");
$Mb = number_format($Mb, "0");
$MB = number_format($MB, "0");
$Gb = number_format($Gb, "0");
$GB = number_format($GB, "0");

echo '
<div class="ui-widget">
    <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
        <strong>Calculations Complete</strong> <br>
             For '.$Listeners.' listeners at '.$StreamBR.' kbps <br>
             Usage @ '.$Timeslot.'<br>
             Kilobits       = '.$Kb.'<br>
             Kilobytes      = '.$KB.'<br>
             Megabits       = '.$Mb.' <br>
             Megabytes      = '.$MB.'<br>
             Gigabits       = '.$Gb.' <br>
             Gigabytes      = '.$GB.'<br>
        </p>
        <form method="POST" id="CLEAR_BWUSAGE">
          <button id="clearusage" name="clear_usage" value="clear_bwusage" type="submit">Clear</button>
        </form>
    </div>
</div>
    


';
}else{
// tell them if they left something out
            echo '
            <div class="ui-widget">
                    <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                        <strong>Alert:</strong> Calculations left out, please enter your calculations in..</p>
                    </div>
            </div>
            ';
}    
    
}


function __about(){
    $jdb = dirname(__FILE__).'/../js/data.json';
    $fp = fopen($jdb,r);
     $fbtabstatus     = $_REQUEST["fbtab"];   
      $jsrc =  fread($fp,filesize($jdb));
       
       $result = json_decode($jsrc,true);
       
       $AppName     = $result['AppName'];
       $Author      = $result['Author'];
       if($fbtabstatus == 'true'){
          $Version = $result['Version'] . ' Facebook Tab App';
       }else{
       $Version     = $result['Version'];
       }
       $LinkedIn    = $result['LinkedIn'];
       $ReleaseD    = $result['Release Date'];
       //print_r($result);

        
	echo '
<script>
$(function() {

   $( "#changelog" ).accordion({
   collapsible: true,
   heightStyle: "content" 
   }); 
   
});
</script> 
<div id="About" class="about">
                       <h2>About '.$AppName.'</h2>
                       <font class="author">Author: '.$Author.'</font><br />
                       <font class="about">Website: <a target="_blank" href="http://casterclub.com">casterclub.com</a></font><br />
                       <font class="version">Version: '.$Version.'</font><br />
                       <font class="contract">Contract me: <a href="'.$LinkedIn.'" target="_blank">'.$LinkedIn.'</a></font><br />
                       <font class="release">Release Date: '.$ReleaseD.'</font><br />
                       <fb:login-button show-faces="true" width="200" max-rows="1"></fb:login-button>';
      //get change log items
             $chl = $result['ChangeLog'];
             echo '<h3>ChangeLog</h3><div id="changelog">';
       foreach($chl as $chlitem){
      
           $d = $chlitem['date'];
           $a = $chlitem['author'];
           $m = $chlitem['message'];
           
            echo '<h3>Date: '.$d.'</h3><div>';
            echo '<ul><li>Author: '.$a.'</li>';
            echo '<li>Message: '.$m.'</li>';
            echo '</ul></div>';
       }               
          echo'</div>';


}

function __tab_calc_listeners(){
        $dd = 1;
        echo "$dd<br>";


}

function __tab_calc_needs(){
        $dd = 2;
        echo "$dd<br>";


}

function __tab_calc_usage(){
        $dd = 3;
        echo "$dd<br>";


}

?>
