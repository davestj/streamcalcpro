<?php
//get device
$device_detected = new Mobile_Detect;

   // detect desktop-only OS: Windows or Mac
   $userAgent = $device_detected->getUserAgent();
   
   if(strpos($userAgent,"Windows NT") !== FALSE) {
      $isWindowsDesktop = 'true';
   }
   else {
      $isWindowsDesktop = 'false';   
   }
   
   if(strpos($userAgent,"Windows NT 6") !== FALSE) {
      $isIE = 'true';
   }else {
      $isIE = 'false';   
   }
   
   if(strpos($userAgent,"MSIE 8") !== FALSE){
      $isIE8 = 'true';  
   }else {
      $isIE8 = 'false';   
   }
   
   
   if(preg_match('/\bFirefox\b/',$userAgent)){
      $isFF = TRUE;   
   }else{
      $isFF = FALSE;   
   }
   
   if(preg_match('/\bChrome\b/',$userAgent)){
      $isCHROME = 'true';   
   }else{
      
      $isCHROME = 'false';   
   }
   
   
   
   if(strpos($userAgent,"Mac OS X") !== FALSE) {
      $isMacDesktop = 'true';
   }
   else {
      $isMacDesktop = 'false';   
   }
   
   if(strpos($userAgent,"Linux") !== FALSE) {
      $isLinuxDesktop = 'true';
   }
   else {
      $isLinuxDesktop = 'false';   
   }

   // detect aspects of mobile devices
    if( $device_detected->isiOS() ){
       $isiOs = "Yes";
       $ver = "iOs version " . $device_detected->version('iPhone');
    }else{
        
        $isiOs = "No";
    }
  
    if( $device_detected->isAndroidOS() ){
        $isAndroid  = "Yes";
        $ver = "Android version " . $device_detected->version('Android');
    }else{

        $isAndroid  = "No";
    }
    
    if ( $device_detected->isMobile() ) {
       $isMobile    = "Yes";
    }else{
       $isMobile    = "No";
    }
     
    // Any tablet device.
    if( $device_detected->isTablet() ){
      $isTablet     = "Yes";
    }else{

      $isTablet     = "No";
    }
    
   
    
    
?>